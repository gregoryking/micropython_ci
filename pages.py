import os
from pathlib import Path
from collections import OrderedDict
from mako.template import Template

vals = {}
vals['hash'] = os.environ.get('LONG_HASH', '')
vals['pipeline_id'] = os.environ.get('CI_PIPELINE_ID', '')

project_url = os.environ.get('CI_PROJECT_URL', 'https://gitlab.com/gregoryking/micropython_ci')
vals['project_url'] = project_url

jobs = OrderedDict()
for artifact in sorted(Path(__file__).parent.glob('micropython_*')):
    if not artifact.is_dir():
        continue
    try:
        _mpy, port, *job, vers = artifact.name.split("_")
        if port not in jobs:
            jobs[port] = []
        job = '_'.join(job)
        url = f'{project_url}/-/jobs/artifacts/master/download?job={job}'
        jobs[port].append((job, url))
    except:
        print("FAILED")
        print(artifact.name, artifact)

vals['jobs'] = jobs


mytemplate = Template('''
<html>
<body>
    <h1>MicroPython @ <a href="https://github.com/micropython/micropython/tree/${hash}">${hash[0:8]}</a></h1>
    CI Build Pipeline: <a href="https://gitlab.com/gregoryking/micropython_ci/pipelines/${pipeline_id}">${pipeline_id}</a>
    <br/>
    <a href="javascript.html">micropython.js live</a>
    <p/>
    % for port, boards in jobs.items():
        <h3>${port.upper()}</h3>
        <ul>
        % for board, url  in boards:
            <li><a href="${url}">${board}</a></li>
        % endfor
        </ul>
    % endfor
</body>
</html>
''')


print(mytemplate.render(**vals))
